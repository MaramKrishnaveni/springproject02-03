package com.userdetails.service;

import com.userdetails.dto.UserDto;
import com.userdetails.dto.UserResponseDto;

public interface UserService {

	UserResponseDto addUser(UserDto userDto);
}
