package com.userdetails.service;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.userdetails.dto.UserDto;
import com.userdetails.dto.UserResponseDto;
import com.userdetails.entity.User;
import com.userdetails.repository.UserRepository;
@Service
public class UserServiceImplementation implements UserService {

	@Autowired
	UserRepository userRepository;
	@Override
	public UserResponseDto addUser(UserDto userDto) {
	
		User user= new User();
		BeanUtils.copyProperties(userDto, user);
		userRepository.save(user);
		UserResponseDto responseDto= new UserResponseDto();
		responseDto.setStatusCode(200);
		responseDto.setStatusMessage("User is added to h2 DB successfully");
		return responseDto;
	}

}
