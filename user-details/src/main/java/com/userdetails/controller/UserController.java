package com.userdetails.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.userdetails.dto.UserDto;
import com.userdetails.dto.UserResponseDto;
import com.userdetails.service.UserService;

@RestController
public class UserController {
	@Autowired
	UserService userService;
//	@RequestMapping("/")
//public String home() {
//	return "home.jsp";
//}
//	
	@PostMapping("/adduser")
	public ResponseEntity<UserResponseDto> newUser(@RequestBody UserDto userDto) {
		UserResponseDto userResponseDto= userService.addUser(userDto);
		return new ResponseEntity<UserResponseDto>(userResponseDto,HttpStatus.OK);
		
	}
	
	
}
